# Práctica 1 - Compilación de software desde código fuente

 - Equipo: JMCE-VVDA-MLOE
 - Programa: [GNU Chess](https://ftp.gnu.org/pub/gnu/chess/)

## La estrcutura de la carperta al compilar el programa es la siguiente

```
.
├── asciinema.cast
├── gnuchess-6.2.8
│   ├── ABOUT-NLS
│   ├── acinclude.m4
│   ├── aclocal.m4
│   ├── ar-lib
│   ├── AUTHORS
│   ├── ChangeLog
│   ├── compile
│   ├── config.guess
│   ├── config.log
│   ├── config.rpath
│   ├── config.status
│   ├── config.sub
│   ├── configure
│   ├── configure.ac
│   ├── COPYING
│   ├── depcomp
│   ├── doc
│   │   ├── fdl.texi
│   │   ├── gnuchess.info
│   │   ├── gnuchess.texi
│   │   ├── Makefile
│   │   ├── Makefile.am
│   │   ├── Makefile.in
│   │   ├── mdate-sh
│   │   ├── quotes.rb
│   │   ├── stamp-vti
│   │   ├── texinfo.tex
│   │   └── version.texi
│   ├── INSTALL
│   ├── install-sh
│   ├── lib
│   │   └── gettext.h
│   ├── m4
│   │   ├── gettext.m4
│   │   ├── iconv.m4
│   │   ├── intlmacosx.m4
│   │   ├── lib-ld.m4
│   │   ├── lib-link.m4
│   │   ├── lib-prefix.m4
│   │   ├── nls.m4
│   │   ├── po.m4
│   │   └── progtest.m4
│   ├── Makefile
│   ├── Makefile.am
│   ├── Makefile.in
│   ├── man
│   │   ├── gnuchess.1
│   │   ├── Makefile
│   │   ├── Makefile.am
│   │   └── Makefile.in
│   ├── misc
│   │   ├── gnuchess.eng
│   │   └── gnuchess.png
│   ├── missing
│   ├── NEWS
│   ├── po
│   │   ├── boldquot.sed
│   │   ├── ChangeLog
│   │   ├── da.gmo
│   │   ├── da.po
│   │   ├── de.gmo
│   │   ├── de.po
│   │   ├── en@boldquot.header
│   │   ├── en@quot.header
│   │   ├── eo.gmo
│   │   ├── eo.po
│   │   ├── es.gmo
│   │   ├── es.po
│   │   ├── fr.gmo
│   │   ├── fr.po
│   │   ├── gl.gmo
│   │   ├── gl.po
│   │   ├── gnuchess.pot
│   │   ├── insert-header.sin
│   │   ├── it.gmo
│   │   ├── it.po
│   │   ├── LINGUAS
│   │   ├── Makefile
│   │   ├── Makefile.in
│   │   ├── Makefile.in.in
│   │   ├── Makevars
│   │   ├── nb.gmo
│   │   ├── nb.po
│   │   ├── nl.gmo
│   │   ├── nl.po
│   │   ├── POTFILES
│   │   ├── POTFILES.in
│   │   ├── pt_BR.gmo
│   │   ├── pt_BR.po
│   │   ├── quot.sed
│   │   ├── remove-potcdate.sin
│   │   ├── Rules-quot
│   │   ├── sr.gmo
│   │   ├── sr.po
│   │   ├── stamp-po
│   │   ├── sv.gmo
│   │   ├── sv.po
│   │   ├── uk.gmo
│   │   ├── uk.po
│   │   ├── vi.gmo
│   │   ├── vi.po
│   │   ├── zh_CN.gmo
│   │   └── zh_CN.po
│   ├── README
│   ├── src
│   │   ├── adapter
│   │   │   ├── adapter.cpp
│   │   │   ├── adapter.h
│   │   │   ├── adapter.o
│   │   │   ├── attack.cpp
│   │   │   ├── attack.h
│   │   │   ├── attack.o
│   │   │   ├── board.cpp
│   │   │   ├── board.h
│   │   │   ├── board.o
│   │   │   ├── book.cpp
│   │   │   ├── book.h
│   │   │   ├── book_make.cpp
│   │   │   ├── book_make.h
│   │   │   ├── book_make.o
│   │   │   ├── book_merge.cpp
│   │   │   ├── book_merge.h
│   │   │   ├── book_merge.o
│   │   │   ├── book.o
│   │   │   ├── colour.cpp
│   │   │   ├── colour.h
│   │   │   ├── colour.o
│   │   │   ├── engine.cpp
│   │   │   ├── engine.h
│   │   │   ├── engine.o
│   │   │   ├── epd.cpp
│   │   │   ├── epd.h
│   │   │   ├── epd.o
│   │   │   ├── fen.cpp
│   │   │   ├── fen.h
│   │   │   ├── fen.o
│   │   │   ├── game.cpp
│   │   │   ├── game.h
│   │   │   ├── game.o
│   │   │   ├── hash.cpp
│   │   │   ├── hash.h
│   │   │   ├── hash.o
│   │   │   ├── io.cpp
│   │   │   ├── io.h
│   │   │   ├── io.o
│   │   │   ├── libadapter.a
│   │   │   ├── line.cpp
│   │   │   ├── line.h
│   │   │   ├── line.o
│   │   │   ├── list.cpp
│   │   │   ├── list.h
│   │   │   ├── list.o
│   │   │   ├── main.cpp
│   │   │   ├── main.h
│   │   │   ├── main.o
│   │   │   ├── Makefile
│   │   │   ├── Makefile.am
│   │   │   ├── Makefile.in
│   │   │   ├── move.cpp
│   │   │   ├── move_do.cpp
│   │   │   ├── move_do.h
│   │   │   ├── move_do.o
│   │   │   ├── move_gen.cpp
│   │   │   ├── move_gen.h
│   │   │   ├── move_gen.o
│   │   │   ├── move.h
│   │   │   ├── move_legal.cpp
│   │   │   ├── move_legal.h
│   │   │   ├── move_legal.o
│   │   │   ├── move.o
│   │   │   ├── option.cpp
│   │   │   ├── option.h
│   │   │   ├── option.o
│   │   │   ├── parse.cpp
│   │   │   ├── parse.h
│   │   │   ├── parse.o
│   │   │   ├── pgn.cpp
│   │   │   ├── pgn.h
│   │   │   ├── pgn.o
│   │   │   ├── piece.cpp
│   │   │   ├── piece.h
│   │   │   ├── piece.o
│   │   │   ├── posix.cpp
│   │   │   ├── posix.h
│   │   │   ├── posix.o
│   │   │   ├── random.cpp
│   │   │   ├── random.h
│   │   │   ├── random.o
│   │   │   ├── san.cpp
│   │   │   ├── san.h
│   │   │   ├── san.o
│   │   │   ├── search.cpp
│   │   │   ├── search.h
│   │   │   ├── search.o
│   │   │   ├── square.cpp
│   │   │   ├── square.h
│   │   │   ├── square.o
│   │   │   ├── uci.cpp
│   │   │   ├── uci.h
│   │   │   ├── uci.o
│   │   │   ├── util.cpp
│   │   │   ├── util.h
│   │   │   └── util.o
│   │   ├── components.cc
│   │   ├── components.h
│   │   ├── components.o
│   │   ├── config.h
│   │   ├── config.h.in
│   │   ├── configmake.h
│   │   ├── engine
│   │   │   ├── attack.cpp
│   │   │   ├── attack.h
│   │   │   ├── attack.o
│   │   │   ├── board.cpp
│   │   │   ├── board.h
│   │   │   ├── board.o
│   │   │   ├── book.cpp
│   │   │   ├── book.h
│   │   │   ├── book.o
│   │   │   ├── colour.h
│   │   │   ├── eval.cpp
│   │   │   ├── eval.h
│   │   │   ├── eval.o
│   │   │   ├── fen.cpp
│   │   │   ├── fen.h
│   │   │   ├── fen.o
│   │   │   ├── hash.cpp
│   │   │   ├── hash.h
│   │   │   ├── hash.o
│   │   │   ├── libengine.a
│   │   │   ├── list.cpp
│   │   │   ├── list.h
│   │   │   ├── list.o
│   │   │   ├── main.cpp
│   │   │   ├── main.o
│   │   │   ├── Makefile
│   │   │   ├── Makefile.am
│   │   │   ├── Makefile.in
│   │   │   ├── material.cpp
│   │   │   ├── material.h
│   │   │   ├── material.o
│   │   │   ├── move_check.cpp
│   │   │   ├── move_check.h
│   │   │   ├── move_check.o
│   │   │   ├── move.cpp
│   │   │   ├── move_do.cpp
│   │   │   ├── move_do.h
│   │   │   ├── move_do.o
│   │   │   ├── move_evasion.cpp
│   │   │   ├── move_evasion.h
│   │   │   ├── move_evasion.o
│   │   │   ├── move_gen.cpp
│   │   │   ├── move_gen.h
│   │   │   ├── move_gen.o
│   │   │   ├── move.h
│   │   │   ├── move_legal.cpp
│   │   │   ├── move_legal.h
│   │   │   ├── move_legal.o
│   │   │   ├── move.o
│   │   │   ├── option.cpp
│   │   │   ├── option.h
│   │   │   ├── option.o
│   │   │   ├── pawn.cpp
│   │   │   ├── pawn.h
│   │   │   ├── pawn.o
│   │   │   ├── piece.cpp
│   │   │   ├── piece.h
│   │   │   ├── piece.o
│   │   │   ├── posix.cpp
│   │   │   ├── posix.h
│   │   │   ├── posix.o
│   │   │   ├── protocol.cpp
│   │   │   ├── protocol.h
│   │   │   ├── protocol.o
│   │   │   ├── pst.cpp
│   │   │   ├── pst.h
│   │   │   ├── pst.o
│   │   │   ├── pv.cpp
│   │   │   ├── pv.h
│   │   │   ├── pv.o
│   │   │   ├── random.cpp
│   │   │   ├── random.h
│   │   │   ├── random.o
│   │   │   ├── recog.cpp
│   │   │   ├── recog.h
│   │   │   ├── recog.o
│   │   │   ├── search.cpp
│   │   │   ├── search_full.cpp
│   │   │   ├── search_full.h
│   │   │   ├── search_full.o
│   │   │   ├── search.h
│   │   │   ├── search.o
│   │   │   ├── see.cpp
│   │   │   ├── see.h
│   │   │   ├── see.o
│   │   │   ├── sort.cpp
│   │   │   ├── sort.h
│   │   │   ├── sort.o
│   │   │   ├── square.cpp
│   │   │   ├── square.h
│   │   │   ├── square.o
│   │   │   ├── trans.cpp
│   │   │   ├── trans.h
│   │   │   ├── trans.o
│   │   │   ├── util.cpp
│   │   │   ├── util.h
│   │   │   ├── util.o
│   │   │   ├── value.cpp
│   │   │   ├── value.h
│   │   │   ├── value.o
│   │   │   ├── vector.cpp
│   │   │   ├── vector.h
│   │   │   └── vector.o
│   │   ├── frontend
│   │   │   ├── atak.cc
│   │   │   ├── atak.o
│   │   │   ├── cmd.cc
│   │   │   ├── cmd.o
│   │   │   ├── common.h
│   │   │   ├── debug.cc
│   │   │   ├── debug.o
│   │   │   ├── engine.cc
│   │   │   ├── engine.o
│   │   │   ├── epd.cc
│   │   │   ├── epd.o
│   │   │   ├── genmove.cc
│   │   │   ├── genmove.o
│   │   │   ├── init.cc
│   │   │   ├── init.o
│   │   │   ├── inlines.h
│   │   │   ├── input.cc
│   │   │   ├── input.o
│   │   │   ├── lexpgn.cc
│   │   │   ├── lexpgn.h
│   │   │   ├── lexpgn.ll
│   │   │   ├── lexpgn.o
│   │   │   ├── libfrontend.a
│   │   │   ├── Makefile
│   │   │   ├── Makefile.am
│   │   │   ├── Makefile.in
│   │   │   ├── move.cc
│   │   │   ├── move.o
│   │   │   ├── output.cc
│   │   │   ├── output.o
│   │   │   ├── pgn.cc
│   │   │   ├── pgn.o
│   │   │   ├── players.cc
│   │   │   ├── players.o
│   │   │   ├── solve.cc
│   │   │   ├── solve.o
│   │   │   ├── swap.cc
│   │   │   ├── swap.o
│   │   │   ├── util.cc
│   │   │   ├── util.o
│   │   │   └── version.h
│   │   ├── GCint.h
│   │   ├── gnuchess
│   │   ├── gnuchess.ini
│   │   ├── gnuchessu
│   │   ├── gnuchessx
│   │   ├── main.cc
│   │   ├── main.o
│   │   ├── Makefile
│   │   ├── Makefile.am
│   │   ├── Makefile.in
│   │   ├── smallbook.bin
│   │   └── stamp-h1
│   ├── TODO
│   └── ylwrap
├── gnuchess-6.2.8.tar.gz
├── Makefile
├── README.md
└── test.exp
```
Donde **src/** es la carpeta donde se encuentra el programa 

### Versión en la que se corrió la práctica 
Linux debian 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64 GNU/Linux


### Instrucciones para instalar:

 1. Abrir una terminal.
 2. Ejecutar `make install`.

### Instrucciones para ejecutar pruebas:

 1. Abrir una terminal
 2. Ejecutar `make test`.

Una vez que este corriendo el programa, podra notar que aparecerá por la terminal
la simulación de un tablero de ajedrez con los respectivos movimientos del test.

```
GNU Chess 6.2.8
White (1) : Can't open file "/usr/local/share/gnuchess/gnuchess.ini": No such file or directory - using defaults
g2g4
1. g2g4

black  KQkq  g3
♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜ 
♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟ 
                
                
            ♙   
                
♙ ♙ ♙ ♙ ♙ ♙   ♙ 
♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖ 
  
Thinking...

white  KQkq  d6
♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜ 
♟ ♟ ♟   ♟ ♟ ♟ ♟ 
                
      ♟         
            ♙   
                
♙ ♙ ♙ ♙ ♙ ♙   ♙ 
♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖ 
  

My move is : d5
```

### Objetivos de la práctica
* Makefile que automatice el proceso de compilación:
    - Instalar el programa y sus archivos resultantes en ${HOME}/local/bin para poder correrlo desde donde sea

* Script con pruebas de acuerdo al software:
    - Hacer pruebas para corroborar el buen funcionamiento del programa y una instalación exitosa

### Autores
* ** César Jardines **  
* ** Diego Villalpando** 
