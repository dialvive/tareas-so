# Bryan González Arreguín

- Número de cuenta: `315136229`

Hola, esta es mi carpeta para la [*tarea-0*](https://redes-ciencias-unam.gitlab.io/2022-1/tareas-so/entrega/tarea-0).

Actividades a las que me quiero dedicar al salir de la facultad:

- Desarrollo en Java
- Modelado de Figuras 3D

Cosas que me gusta hacer en mi tiempo libre:

- Escuchar musica
- Jugar videojuegos
- Experimentos en plugins de un juego

| Esta es la imagen que elegí |
|:---------------------------:|
| ![](img/bryandt01.png)      |

